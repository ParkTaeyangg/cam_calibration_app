#include "worker.h"
#include <iostream>

Worker::Worker()
{
    m_start = false;
}

void Worker::run()
{
    cv::Mat frame;
    cv::Mat gray;
    cv::VideoCapture cap(2);
    if (!cap.isOpened())                        //카메라 연결 안 되어 있을 경우
    {
       qDebug("please connect camera");
        exit();
    }

    while(1)
    {
        cap>>frame;
        if( frame.empty())
          {
            cv::resize( frame, frame, cv::Size(480, 640) );
            frameFinished(frame);
            break;
          }

//        cv::imshow("gg",frame);          //출력

        if(m_start)
        {
            break;
        }
        frameFinished(frame.clone());
        QThread::msleep(30);
    }
}
