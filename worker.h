#ifndef WORKER_H
#define WORKER_H
#include <QThread>
#include <QObject>
#include <opencv2/opencv.hpp>
#include <QDebug>

class Worker : public QThread
{
    Q_OBJECT
public:
    Worker();
    bool m_start;
private:
    void run();
signals:
//    void ShowVideo(cv::Mat frame);
    void frameFinished(cv::Mat);
};

#endif // WORKER_H
