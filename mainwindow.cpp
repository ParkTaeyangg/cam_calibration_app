#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPixmap>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    checked = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::ShowVideo(cv::Mat frame)
{
    QPixmap p=QPixmap::fromImage(QImage(frame.data, frame.cols, frame.rows,
              frame.step,QImage::Format_RGB888).rgbSwapped());
    ui->label->setPixmap(p.scaled(640,480,Qt::KeepAspectRatio));
//    ui->label->show();
}

void MainWindow::on_cam_off_button_clicked()
{
    if(checked==1)
        checked=0;
    else
        checked=1;

    if(checked)
    {
        worker = new Worker();
        qRegisterMetaType<cv::Mat>("cv::Mat");
        connect(worker, SIGNAL(frameFinished(cv::Mat)),this, SLOT(ShowVideo(cv::Mat)));
        worker->start();
//        connect(worker, SIGNAL(ShowVideo(cv::Mat)),this, SLOT(ShowVideo(cv::Mat)));
    }
    else
    {
        worker->m_start=true;
    }
}

void MainWindow::on_Exit_button_clicked()
{
    worker->m_start=true;
    QThread::usleep(1000);
    this->close();
}
