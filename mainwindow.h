#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <worker.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    int checked;
private slots:
//    void on_cam_button_clicked();
    void on_cam_off_button_clicked();
    void ShowVideo(cv::Mat frame);


    void on_Exit_button_clicked();

private:
    Ui::MainWindow *ui;
    Worker *worker;
};

#endif // MAINWINDOW_H
